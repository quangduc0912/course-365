const express = require('express');
const mongoose = require('mongoose');
const path = require('path');

const app = express();
const port = 3000;

//import router
const courseRouter = require('./app/routes/courseRouter');

//cấu hình để app đọc được body request dạng json
app.use(express.json());

//cấu hình để app đọc được tiếng việt (UTF8)
app.use(express.urlencoded({ extended : true }));

//middleware static
app.use(express.static(__dirname + '/src'));

//router-level middleware
app.use('/devcamp-course365', courseRouter);


app.get('/devcamp-course365', (req,res) => res.sendFile(path.join(__dirname, './src/browseCourses.html')));

//connect mongoDB 
const uri = 'mongodb://localhost:27017/COURSE_365';
mongoose.connect(uri, (error) => {
    if (error) throw error;
    console.log('Successfully connected');
   })


app.listen(port, () => console.log(`App listening to port ${port}`));
